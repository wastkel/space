﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour, IWeapon
{

    [SerializeField] float damage;
    [SerializeField] float projectileSpeed;
    [SerializeField] float fireRate;
    [SerializeField] float weight;
    [SerializeField] string nameWeapon;
    [SerializeField] GameObject projectileType;
    [SerializeField] Transform[] muzzles;
    [SerializeField] string typeWeapon;

    float nextShot;
    public float Damage { get { return damage; } set { } }
    public float ProjectileSpeed { get { return projectileSpeed; } set { } }
    public float Weight { get { return weight; } set { } }
    public string NameWeapon { get { return nameWeapon; } set { } }
    public GameObject ProjectileType { get { return projectileType; } set { } }
    public Transform[] Muzzles { get { return muzzles; } set { } }
    public string TypeWeapon { get { return typeWeapon; } set { } }
    public float FireRate { get { return 1000 / fireRate / 1000; } set { } }


    private void Awake()
    {
         
    }

    void Start()
    {

    }


    void Update()
    {
    }

    public void Shoot()
    {
        if (Time.time > nextShot)
        {
            nextShot = Time.time + FireRate;
            foreach (var muzzle in muzzles)
            {
                var projectile = Instantiate(ProjectileType, muzzle.position, transform.rotation);
                projectile.GetComponent<IProjectile>().SetSpeed(ProjectileSpeed);
                //projectile.GetComponent<Rocket>().SetTarget(player.hit.transform);//заглушка
            }
        }
    }
}
