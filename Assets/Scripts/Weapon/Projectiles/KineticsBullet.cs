﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KineticsBullet : MonoBehaviour, IProjectile
{
    float Speed { get; set; }

    void Start()
    {
        Destroy(gameObject, 3);// временное решение до пула
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
    }

    public void SetSpeed(float speed)
    {
        Speed = speed;
    }
}
