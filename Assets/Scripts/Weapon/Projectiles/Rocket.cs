﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour, IProjectile
{
    #region Огромный костыль, потом удалю
    float Speed { get; set; }
    Transform Target { get; set; }
    Rigidbody rb;
    [SerializeField] float speedRotation = 50f;
    float SpeedRotation { get; set; }
    [SerializeField] float delay = 0.5f;

    void Start()
    {
        Invoke("SetRotation", delay);
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, 3);
    }

    void Update()
    {
        rb.AddRelativeForce(Vector3.forward * Speed);
        if (Target != null)
        {
            var rotation = Quaternion.LookRotation(Target.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, SpeedRotation * Time.deltaTime);
        }
    }

    public void SetSpeed(float speed)
    {
        Speed = speed;
    }

    public void SetTarget(Transform target)
    {
        Target = target;
    }

    public void SetRotation()
    {
        SpeedRotation = speedRotation;
    }
    #endregion
}
