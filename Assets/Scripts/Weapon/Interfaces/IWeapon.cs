﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeapon
{
    float Damage { get; set; }
    float ProjectileSpeed { get; set; }
    float FireRate { get; set; }
    float Weight { get; set; }
    string NameWeapon { get; set; }
    GameObject ProjectileType { get; set; }
    Transform[] Muzzles { get; set; }
    string TypeWeapon { get; set; }
    void Shoot();
}
