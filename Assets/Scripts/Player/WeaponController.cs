﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    IWeapon[] weapons;

    void Start()
    {
        weapons = GetComponentsInChildren<IWeapon>();
    }

    public void Shoot()
    {
        foreach (var weapon in weapons)
        {
            weapon.Shoot();
        }
    }

    void Update()
    {

    }
}


