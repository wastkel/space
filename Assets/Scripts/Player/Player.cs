﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    IInputController InputController;
    List<IInputController> inputControllers;


    //заглушка
    bool space = true;

    [SerializeField] GameObject SpaceShip;
    [SerializeField] GameObject BattleShip;
    [SerializeField] GameObject SpaceArea;
    [SerializeField] GameObject BattleArea; 

    //заглушка

    private void Awake()
    {
        inputControllers = new List<IInputController>();
        inputControllers.Add(GetComponent<SpaceInputController>());
        inputControllers.Add(GetComponent<BattleInputController>());
    }

    void Start()
    {
        Kostil(); //заглушка
    }

    void Update()
    {
        

        if (Input.GetKeyUp(KeyCode.Space)) //заглушка
        {
            space = !space;
            Kostil();
        }

        if (Input.GetMouseButton(0)) //переделаю под паттерн команду
        {
            InputController.OnClick();
        }

        //InputController.Move(inputVector);
    }

    private void Kostil()//этот ужас нужен для тестов функционала
    {
        Debug.Log("start");
        if (space)
        {
            foreach (var item in inputControllers)
            {
                Debug.Log("Space "+item.Name);
                if (item.Name == "Space")
                {
                    InputController = item;
                    ((Behaviour)item).enabled = true;
                    SpaceShip.SetActive(true);
                    BattleShip.SetActive(false);
                    SpaceArea.SetActive(true);
                    BattleArea.SetActive(false);
                }
                else
                    ((Behaviour)item).enabled = false;
            }

        }
        else
        {
            foreach (var item in inputControllers)
            {
                Debug.Log(item.Name);
                if (item.Name == "Battle")
                {
                    InputController = item;
                    ((Behaviour)item).enabled = true;
                    SpaceShip.SetActive(false);
                    BattleShip.SetActive(true);
                    SpaceArea.SetActive(false);
                    BattleArea.SetActive(true);
                }
                else
                    ((Behaviour)item).enabled = false;
            }
        }
    }
}
