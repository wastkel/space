﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour
{
    [SerializeField] IEngine engine;

    public float SpeedRotation { get => engine.SpeedRotation; }
    public int Health { get => engine.Health; set => engine.Health = engine.Health <= 0 ? 0 : value; }
    public float Force { get => engine.Force; }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public void AddForce(Rigidbody rb, Vector3 velocity)
    {
        rb.AddRelativeForce(velocity * engine.Force * Time.deltaTime);
    }
}
