﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Engine",menuName = "Add Engine")]
public class IEngine : ScriptableObject
{
    [SerializeField] float force;
    [SerializeField] float speedRotation;
    [SerializeField] int health;

    public int Health { get => health; set => health = health <= 0 ? 0 : value; }
    public float Force { get => force; }
    public float SpeedRotation { get => speedRotation; }
}
