﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputController
{
    string Name { get; }
    void OnClick();
    //void Move(Vector3 vector);
}
