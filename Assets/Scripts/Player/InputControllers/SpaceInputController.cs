﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInputController : MonoBehaviour, IInputController
{
    public string Name { get { return "Space"; }}
    Rigidbody rb;
    Camera mainCam;
    Transform Target { get; set; }
    Engine engine;

    [SerializeField] GameObject StartPlanet;

    [SerializeField] float SpeedRotation = 1.5f;
    [SerializeField] float constSpeedInSpace = 10;
    [SerializeField] float stopDistanceMove = 0.75f;

    Ray ray;

    private void Awake()
    {
        Target = StartPlanet.transform;
        mainCam = Camera.main;
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        engine = GetComponentInChildren<Engine>();
    }

    
    void Update()
    {
        ray = mainCam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 50, Color.green);
        Move();
    }

    //написал что-то страшное в 5 утра! потом перепишу
    public void Move()
    {
        Debug.DrawRay((rb.velocity * 3) + transform.position, ((rb.velocity * 3) + transform.position) + Target.position, Color.red);
        if (Vector3.Distance(Target.position, (rb.velocity * 3) + transform.position) >= stopDistanceMove)
        {
            if (Vector3.Distance(transform.position,((rb.velocity * 3) + transform.position)) <= Vector3.Distance(transform.position,Target.position))
            {
                Debug.DrawRay(transform.position, (rb.velocity * 3), Color.green);
                //transform.position = Vector3.Lerp(transform.position, Target.position, constSpeedInSpace * Time.deltaTime);
                engine.AddForce(rb, Vector3.forward);
                var rotation = Quaternion.LookRotation(Target.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, engine.SpeedRotation * 2 * Time.deltaTime);
            }
            else
            {
                Debug.DrawRay(transform.position, (rb.velocity * 3), Color.blue);
                var rotation = Quaternion.LookRotation(Target.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, engine.SpeedRotation * 2 * Time.deltaTime);
            }
        }
        //else
        //{
        //    Target.GetComponent<ISpaceObject>().OnClick();
        //}
    }

    public void OnClick()
    {
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        if (hit.transform != null)
        {
            Target = hit.transform;
        }
        //hit.transform.GetComponent<Stars>().OnClick(gameObject);
    }

    //public void Enable()
    //{
    //    this.enabled = false;
    //}
}
