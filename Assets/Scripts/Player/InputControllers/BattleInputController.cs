﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class BattleInputController : MonoBehaviour, IInputController
{
    public string Name { get { return "Battle"; }}
    Rigidbody battleRigidbody;
    Camera mainCam;
    Ray ray;
    WeaponController weaponController;
    Engine engine;
    //public RaycastHit hit; // для заглушки
    //[SerializeField] Text text;

    [SerializeField] List<IWeapon> weapons;
    [SerializeField] float speed = 250;


    Vector3 inputVector;
    Vector3 rotationVector;

    private void Awake()
    {
        
    }

    void Start()
    {
        mainCam = Camera.main;
        battleRigidbody = GetComponent<Rigidbody>();
        weaponController = GetComponentInChildren<WeaponController>();
        engine = GetComponentInChildren<Engine>();
    }

    
    void FixedUpdate()
    {
        //ray = mainCam.ScreenPointToRay(Input.mousePosition);
        Move();
        //text.text = battleRigidbody.velocity.ToString();
    }

    public void OnClick()
    {
        weaponController.Shoot();
        //OnClickTarget();
    }

    public void Move()
    {
        inputVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        engine.AddForce(battleRigidbody, inputVector);
        rotationVector = new Vector3(0, Input.GetAxisRaw("Rotation"), 0);
        transform.rotation = Quaternion.Euler(rotationVector) * transform.rotation;
    }

    public void OnClickTarget()//заглушка
    {
        
        //Physics.Raycast(ray, out hit);
        //Debug.Log(string.Format("Цель {0} захвачена!", hit.transform.name));
    }

    void GetEquipments()
    {
        //GetComponent
    }
}
