﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float hight = 100;
    
    void Start()
    {
    }

    void Update()
    {
        transform.position = Vector3.Slerp(transform.position, new Vector3(target.position.x, hight, target.position.z - hight * 0.75f), 1);
    }
}
